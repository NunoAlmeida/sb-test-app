import React, { Component } from 'react';
import './Event.scss';
import PrimaryMarket from '../PrimaryMarket/PrimaryMarket';
import expand from '../images/expand-button.png';
import more from '../images/more.png';
import { ws } from '../webSocketLib';

class Event extends Component {
	constructor(props) {
		super(props);
		this.state = {
			listOutcomes: [],
			marketData: null,
			open: false
		}
		this.getMarket = (id) => {
			ws.send(JSON.stringify({ type: 'getMarket', id: id }))
			this.setState({ open: !this.state.open })

		};
		this.getOutcomes = (market) => {
			this.setState({ listOutcomes: market.outcomes })
			market.outcomes.forEach(id => {
				ws.send(JSON.stringify({ type: 'getOutcome', id: id }))
			});
		};
		this.addOutcomesToMarker = (outcome) => {
			const listUpdated = this.state.listOutcomes.map(id => {
				if (id === outcome.outcomeId) return outcome;
				else return id;
			})
			this.setState({ listOutcomes: listUpdated })
		};
	}

	componentDidMount() {
		ws.addEventListener("message", m => {
			const { type, data } = JSON.parse(m.data);
			if (data.marketId === this.props.event.markets[0]) {
				if (type === 'MARKET_DATA' && this.props.inListScreen) {
					this.setState({ marketData: data })
					this.getOutcomes(data)
				} else if (type === 'OUTCOME_DATA' && this.props.inListScreen) {
					this.addOutcomesToMarker(data)
				}
			}
		})
	}

	outcomesReady() {
		if (this.state.listOutcomes.length && this.state.open) {
			const ready = !this.state.listOutcomes.some(outcome => !isNaN(outcome))
			return ready ? true : false
		}
	}

	render() {
		const { event } = this.props
		return (
			<div className='event-block' key={event.eventId}>
				<div className='event-header' key={`event_${event.eventId}`}>
					<h4 className='event-title' key={`title_${event.eventId}`}>{event.name}</h4>
					<div className='more-market' onClick={() => this.getMarket(event.markets[0])} key={`expand_${event.eventId}`}>
						<img className={(this.state.open) ? ('img-open') : ('')} src={expand} alt="" key={`img_expand_${event.eventId}`} ></img>
					</div>
					<div className='more-market' onClick={() => this.props.handlerEventsDetails(event.eventId)} key={`more_${event.eventId}`}>
						<img src={more} alt="" key={`img_more_${event.eventId}`} ></img>
					</div>
				</div>
				{(this.outcomesReady()) ? <PrimaryMarket {...this.state}  {...this.props} /> : undefined}
			</div>
		);
	}
}

export default Event;


