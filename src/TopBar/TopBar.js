import React, { Component } from 'react';
import logo from '../images/betstars-logo-alt.png';
import './TopBar.scss';

class TopBar extends Component {
	constructor(props) {
		super(props);
		this.state = {
			connection: 0,
			message: {},
			listOfEvents: null
		};
	}

	decimalTypeOrFractionText(price) {
		if (this.props.decimalType)
			return `Decimal`
		else
			return `Fraction`;
	}

	render() {
		return (
			<div id="top-bar">
				<img id='logo' src={logo} alt="" />
				<div id='top-bar-body'><div className='button_decimal' onClick={() => this.props.handlerTypeOfOdds(!this.props.decimalType)}>{this.decimalTypeOrFractionText()}</div></div>
			</div>
		);
	}
}

export default TopBar;
