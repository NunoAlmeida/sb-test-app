import React, { Component } from 'react';
import './EventDetails.scss';
import Market from '../Market/Market'
import { ws } from '../webSocketLib';

class Event extends Component {
	constructor(props) {
		super(props);
		this.state = {
			listOfMakerts: []
		}
	}

	componentDidMount() {
		ws.addEventListener("message", m => {
			const { type, data } = JSON.parse(m.data);
			if (type === 'MARKET_DATA') {
				const newListOfMakert = [data, ...this.state.listOfMakerts]
				this.setState({ listOfMakerts: newListOfMakert });
			}
		})
		this.props.eventDetails.markets.forEach(id => {
			ws.send(JSON.stringify({ type: 'getMarket', id: id }))
		})
	}


	filtredList(listOfMarkets) {

		if (listOfMarkets.length === this.props.eventDetails.markets.length) {
			let filtredList = listOfMarkets.filter(marker => marker.status.active && marker.status.displayable)
			filtredList.sort((a, b) => {
				if (a.displayOrder === b.displayOrder) {
					let nameA = a.name.toUpperCase();
					let nameB = b.name.toUpperCase();
					if (nameA < nameB) return -1;
					if (nameA > nameB) return 1;
				}
				return a.displayOrder - b.displayOrder;
			})
			filtredList = filtredList.map((market, index) => {
				if (index < 10) market.tabOpen = true;
				else market.tabOpen = false;
				return market;
			})
			return filtredList;
		}
		return [];
	}

	render() {
		const { competitors, scores, typeName, linkedEventTypeName, startTime } = this.props.eventDetails;
		return (
			<div id='event_details'>
				<div id='event_details_header'>
					<div id='scoreboard'>
						<div id='startTime'>{`Start time: ${new Date(startTime).toUTCString().slice(0, -4)}`}</div>
						<div id='teams'>
							<div id='home_team' className='teamTitle'>{competitors[0].name}</div>
							<div id='away_team' className='teamTitle'>{competitors[1].name}</div>
						</div>
						<div id='scores'>
							<div id='home_score' className='score'>{scores[competitors[0].position]}</div>
							<div id='away_score' className='score'>{scores[competitors[1].position]}</div>
						</div>
					</div>
					<div id='other_details'>
						<div id='type'>{typeName}</div>
						<div id='league'>{linkedEventTypeName}</div>
					</div>
					<div id='return_button' onClick={this.props.handlerReturn}>{`< Back`}</div>

				</div >
				<div id='markets_list'>
					{this.filtredList(this.state.listOfMakerts).map(market => {
						return <Market market={market} oddsConversion={this.props.oddsConversion} key={market.marketId} />
					}
					)}
				</div>
			</div>
		);
	}
}

export default Event;