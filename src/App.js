import React, { Component } from 'react';
import Dashboard from './Dashboard/Dashboard';
import TopBar from './TopBar/TopBar';
import { ws } from './webSocketLib';
import './App.scss';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      decimalType: false
    };
    this.handlerTypeOfOdds = this.handlerTypeOfOdds.bind(this)
  }

  handlerTypeOfOdds(type) {
    this.setState({ decimalType: type })
  }

  componentWillMount() {
    ws.addEventListener("message", m => {
      const data = JSON.parse(m.data)
      if (data.type === "INIT") {
        this.setState({ connection: true })
      }
    })
  }

  oddsConversion(price) {
    if (this.state.decimalType)
      return price.decimal;
    else
      return `${price.num}/${price.den}`;
  }


  render() {
    return (
      <div id='app' >
        <div id='app-header'><TopBar handlerTypeOfOdds={this.handlerTypeOfOdds} decimalType={this.state.decimalType} /></div>
        <div id='dashboard-container'>
          {this.state.connection ? <Dashboard oddsConversion={this.oddsConversion.bind(this)} /> : <p>Connecting...</p>}
        </div>
      </div>
    );
  }
}

export default App;