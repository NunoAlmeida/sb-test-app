import React, { Component } from 'react';
import './Market.scss';
import expand from '../images/expand-button.png';
import { ws } from '../webSocketLib';

class Market extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tabOpen: false,
			listOfOutcome: []
		}
	}

	componentDidMount() {
		this.setState({ tabOpen: this.props.market.tabOpen });

		ws.addEventListener("message", m => {
			const { type, data } = JSON.parse(m.data);
			if (type === 'OUTCOME_DATA' && data.marketId === this.props.market.marketId) {
				this.setState({ listOfOutcome: [data, ...this.state.listOfOutcome] });
			}
		})

		if (this.props.market.tabOpen) {
			this.props.market.outcomes.forEach(id => {
				ws.send(JSON.stringify({ type: 'getOutcome', id: id }))
			})
		}
	}

	getMarket() {
		this.setState({ tabOpen: !this.state.tabOpen });
		if (!this.state.tabOpen) {
			this.setState({ listOfOutcome: [] });
			this.props.market.outcomes.forEach(id => {
				ws.send(JSON.stringify({ type: 'getOutcome', id: id }))
			})
		}

	}

	displayOutcomes(market) {
		if (this.state.tabOpen && this.state.listOfOutcome.length === market.outcomes.length) {
			return (<div id='outcome_box' key={`outcomes_${market.marketId}`}>
				{this.state.listOfOutcome.map((outcome, index) => {
					return (
						<div className='outcome' key={`${outcome.outcomeId}_${index}`}>
							<label>{outcome.name}</label>
							<p>{this.props.oddsConversion(outcome.price)}</p>
						</div>
					)
				})}
			</div>)
		}
	}


	render() {
		const { market } = this.props;
		const { marketId, name } = market;
		return (
			<div id='market_container' key={`market_box_${marketId}`}>
				<div id='market_header' key={`market_header_${marketId}`}>
					<h6 id='market_title' key={`market_title_${marketId}`}>{name}</h6>
					<div className='more_market' onClick={() => this.getMarket()} key={`expand_${marketId}`}>
						<img className={(this.state.tabOpen) ? ('img-open') : ('')} src={expand} alt="" key={`img_expand_${marketId}`} ></img>
					</div>
				</div>
				{this.state.tabOpen ? this.displayOutcomes(market) : undefined}



			</div >
		);
	}
}


export default Market;