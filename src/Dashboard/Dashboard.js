import React, { Component } from 'react';
import './Dashboard.scss';
import Event from '../Event/Event';
import EventDetails from '../EventDetails/EventDetails';
import { ws } from '../webSocketLib';

class Dashboard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			listOfEvents: null,
			inListScreen: true,
			eventIdOnFoco: null,
			eventDetails: {}
		}
		this.handlerReturn = this.handlerReturn.bind(this)
		this.handlerEventsDetails = this.handlerEventsDetails.bind(this)
	}

	handlerReturn() {
		this.setState({ inListScreen: true })
	}

	handlerEventsDetails(eventId) {
		ws.send(JSON.stringify({ type: 'getEvent', id: eventId }))
		this.setState({
			inListScreen: false,
			eventIdOnFoco: eventId
		})
	}

	componentDidMount() {
		ws.addEventListener("message", m => {
			const { type, data } = JSON.parse(m.data);
			if (type === 'LIVE_EVENTS_DATA')
				this.setState({ listOfEvents: data });
			else if (type === 'EVENT_DATA')
				this.setState({ eventDetails: data });

		})
		ws.send(JSON.stringify({ type: 'getLiveEvents', primaryMarkets: true }))
	}


	displayListOfEvents() {
		return (this.state.listOfEvents) ?
			this.state.listOfEvents.map(event => {
				return (<Event
					event={event}
					{...this.state}
					{...this.props}
					handlerEventsDetails={this.handlerEventsDetails}
					key={event.eventId} />)
			}) : 'Loading...'
	}

	checkWhichScreenToDisplay() {
		const { inListScreen, eventDetails, eventIdOnFoco } = this.state;
		if (!inListScreen && eventDetails.eventId === eventIdOnFoco) {
			return <EventDetails
				{...this.state}
				{...this.props}
				handlerReturn={this.handlerReturn}
				key={eventIdOnFoco} />
		} else {
			return this.displayListOfEvents();
		}
	}

	render() {
		return (
			<div id='eventList-screen'>
				{this.checkWhichScreenToDisplay()}
			</div>
		);
	}
}

export default Dashboard;