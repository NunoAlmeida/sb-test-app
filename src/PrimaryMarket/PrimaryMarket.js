import React from 'react';
import './PrimaryMarket.scss';

export default function PrimaryMarket({ marketData, listOutcomes, oddsConversion }) {

	const { name, outcomeId, marketId: id } = marketData;

	return (
		< div className='market-container' key={`market_box_${id}`} >
			<h6 className='market-title' key={`market_title_${id}`}>{name}</h6>
			<div className={`outcome_box`} key={`outcomes_${id}`}>
				{listOutcomes.map((outcome, index) => {
					return (
						<div className='outcome' key={`${outcomeId}_${index}`}>
							<label>{outcome.name}</label>
							<p>{oddsConversion(outcome.price)}</p>
						</div>
					)
				})}
			</div>
		</div >
	);
}



