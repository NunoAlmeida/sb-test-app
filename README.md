## Instructions for how to run :

```
npm install

docker run -it --rm --name sbg-tech-test-api -p 8888-8890:8888-8890 sbgtechtest/api:2.0.0
```

Wait until the docker is serving the API.

```
npm start
```

## What was done:

In this project I concluded exercises 1 and 2 of the 3 proposed challenges.

#### Points that require finishing/improving:

- add unit tests;
- improve code organization;
- create a library for WebSocket functions;
- add error handlers;

#### Considerations:

- This project was developed in React Component, but I considered React Hooks however I didn't know enough to present something in a plausible time.

- Using React Hooks had the advantage of more readable code and decreased component co-toxicity.

- I tried to analyze the hypothesis of using Redux or React Hooks to lessen the complexity of data storing.

- Unfortunately I could not add the tests to the code presented for lack of time and knowledge. My experience in Unit Tests is centered on the backend development that I have developed.

### Conclusion:

I will be happy to continue working on this exercise and add the points announced above.
I intend to study the concepts of React Hooks, Redux, and Frontend Test because I consider them to be my main weaknesses in this exercise.